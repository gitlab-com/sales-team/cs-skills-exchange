This is the Customer Success Skills Exchange Readme! 

The Customer Success Skills Exchange is a weekly enablement session delivered every Wednesday, alrternating between 10:30AM and 2:00PM EST. The session is open to all, but content is prioritized based on customer success team needs. Topics include a mix of functional, soft skill, and technical. 