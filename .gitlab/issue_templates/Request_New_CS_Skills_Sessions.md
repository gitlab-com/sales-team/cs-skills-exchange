**Please take a moment to plan out the learning objectives, collateral, format, & calls to action for this session.**




**Overview:** > *(Please provide a brief overview for this session that articulates why the Customer Success audience will want to attend)*

*

**Date:** 

*  

**Presenters:** > *(If possible please consider inviting a member of the field sales team to speak about thier experiences to support your topic)*

*      

**Learning Objectives:** > *(Please write 2-3 key takeaways you would like for the audience to be infomed about as a result of the session)*

1. 
2. 
3. 

**Resources:** 

*  Handbook:

*  Other Collateral: 

**Session Format:** > *(Time for presenting vs. taking questions and format during this 50 minute session)*

- [ ] Interview 
- [ ] Ask Me Anything 
- [ ] Presentation with Q&A
- [ ] Panel Discussion 

**Call to Action:** > *(What action(s) do you want Customer Success team members to take after this session?)*

* TBD

**Polling** > *(Would you like to add a zoom poll for this session?)*

- [ ] Yes
- [ ] No

**If you would like to add a poll to your session please write the poll question and answer choices that should be presented to your audience.*

* TBD 

**Public / Private Recording?:** > *(Would you like for this presentation to be made public or private on YouTube?)*

- [ ] Public 
- [ ] Private 

**Which customer Value Driver(s) apply to this session?** 

- [ ] Increase Operational Efficiences 
- [ ] Deliver Better Products Faster
- [ ] Reduce Security & Compliance Risk
- [ ] N/A 

**Which GitLab Differentiator(s) apply to this session?**

- [ ] Single Application for Entire DevOps Lifecycle 
- [ ] Leading SCM and CI in One Application 
- [ ] Built In Security and Compliance 
- [ ] Deploy Your Software Anywhere
- [ ] Optimized for Kubernetes
- [ ] End-to-End Insight and Visibility
- [ ] Flexible GitLab Hosting Options 
- [ ] Rapid Innovation 
- [ ] Open Source; Everyone Can Contribute
- [ ] Collaborative and Transparent Customer Experience 
- [ ] N/A 

**Should the GitLab Value Framework be updated based on any information presented in this session?**

- [ ] Yes
- [ ] No

**If you answered yes to the above question please explain what should be added or changed in the GitLab Value Framework**

* TBD


**Facilitator Actions**
* [ ]  Confirm speaker availability & learning objectives.
* [ ]  Update [notes document](https://docs.google.com/document/d/1kchnm55N8zx8tBBsxilWadGqBndhvb5d4eG9LsSS6DA/edit?usp=sharing) in calendar invitation. 
* [ ]  Promote the upcoming session on #customer-success Slack channel.
* [ ]  Upload recorded session to GitLab Unfiltered [Customer Success Skills Exchange playlist.](https://www.youtube.com/playlist?list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_)
* [ ]  Trim recording to begin at the start of the session.
* [ ]  Update future & past sessions list with recording link in the [CS Skills Exchange Handbook.](https://about.gitlab.com/handbook/sales/training/customer-success-skills-exchange/#customer-success-skills-exchange-sessions)
* [ ]  Promote the recorded session availability on #customer-success Slack channel. 

/assign @jblevins608
/label ~"CS Skills Exchange - Global"
/label ~"status::plan"
